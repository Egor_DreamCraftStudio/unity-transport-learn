using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Unity.Collections;
using Unity.Networking.Transport;
using UnityEngine;

namespace Multiplayer
{
	public class Client : MonoBehaviour
	{
		[SerializeField] bool _useLocalHost = true;
		[SerializeField] string _ip;

		NetworkDriver _driver;
		NetworkConnection _connection;
		List<ICommandReaction> _commandReactions;
		UniTaskCompletionSource<ConnectionResult> _connectCompletionSource;

		void Update()
		{
			_driver.ScheduleUpdate().Complete();

			if (_connection.IsCreated == false)
				return;

			ProcessConnection();
		}

		void OnDestroy()
		{
			Disconnect();
			_driver.Dispose();
		}

		public UniTask<ConnectionResult> Connect()
		{
			_connectCompletionSource =
				new UniTaskCompletionSource<ConnectionResult>();

			if (TryCreateEndPoint(out var endpoint) == false)
			{
				_connectCompletionSource.TrySetResult(ConnectionResult.Failed);
				return _connectCompletionSource.Task;
			}

			CreateDriver();
			_connection = _driver.Connect(endpoint);

			_commandReactions = new List<ICommandReaction>()
				{ new LogCommand() };

			return _connectCompletionSource.Task;
		}

		public void SendSomeCommand(Command command, string msg)
		{
			_driver.BeginSend(_connection, out var writer);
			writer.WriteUShort((ushort)command);
			writer.WriteFixedString128(msg);
			_driver.EndSend(writer);
		}

		void ProcessConnection()
		{
			NetworkEvent.Type cmd;
			while ((cmd = _connection.PopEvent(_driver, out var stream)) !=
			       NetworkEvent.Type.Empty)
			{
				if (ProcessDisconnect(cmd) == false)
					return;

				ProcessNetworkEvents(cmd, ref stream);
			}
		}

		void ProcessNetworkEvents(NetworkEvent.Type cmd,
			ref DataStreamReader stream)
		{
			switch (cmd)
			{
				case NetworkEvent.Type.Data:
					ProcessData(ref stream);
					break;
				case NetworkEvent.Type.Connect:
					ProcessConnect();
					break;
				case NetworkEvent.Type.Empty:
					Debug.LogWarning($"Has not handler for \"{cmd}\".");
					break;
			}
		}

		void ProcessData(ref DataStreamReader stream)
		{
			var command = Util.GetCommand(ref stream);
			foreach (var reaction in _commandReactions)
				reaction.TryReact(command, ref stream);
		}

		void ProcessConnect()
		{
			_connectCompletionSource.TrySetResult(ConnectionResult.Done);
		}

		void Disconnect()
		{
			_connection.Disconnect(_driver);
			_connection = default;
		}

		bool ProcessDisconnect(NetworkEvent.Type cmd)
		{
			if (cmd == NetworkEvent.Type.Disconnect)
			{
				Debug.Log("Client got disconnected from server.");
				_connection = default;
				return false;
			}

			return true;
		}

		void CreateDriver()
		{
#if UNITY_WEBGL
			_driver = NetworkDriver.Create(new WebSocketNetworkInterface());
#else
			_driver = NetworkDriver.Create(new UDPNetworkInterface());
#endif
		}

		bool TryCreateEndPoint(out NetworkEndpoint endpoint)
		{
			if (_useLocalHost)
			{
				endpoint = NetworkEndpoint.LoopbackIpv4.WithPort(Const.c_Port);
				return true;
			}

			if (NetworkEndpoint.TryParse(_ip, Const.c_Port, out endpoint))
				return true;

			Debug.LogError($"Incorrect ip: \"{_ip}\".");
			return false;
		}
	}
}