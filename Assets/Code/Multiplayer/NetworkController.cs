﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Multiplayer
{
	public class NetworkController : MonoBehaviour
	{
		[SerializeField] Server _server;
		[SerializeField] Client _client;

		void Start()
		{
			_server.Init();
			CreateClientAsync();
		}

		async UniTaskVoid CreateClientAsync()
		{
			await _client.Connect();
			_client.SendSomeCommand(Command.String128, "Hello 12345 🛠.\n new line");
		}
	}
}