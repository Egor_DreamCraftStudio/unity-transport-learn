﻿using Unity.Collections;

namespace Multiplayer
{
	public interface ICommandReaction
	{
		void TryReact(Command command, ref DataStreamReader stream);
	}
}