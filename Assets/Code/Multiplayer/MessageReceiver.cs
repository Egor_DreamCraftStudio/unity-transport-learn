﻿using Unity.Collections;
using UnityEngine;

namespace Multiplayer
{
	public sealed class MessageReceiver : ICommandReaction
	{
		public void TryReact(Command command, ref DataStreamReader stream)
		{
			if (command == Command.String128)
			{
				var msg = stream.ReadFixedString128();
				Debug.Log(msg);
				Debug.Log(msg.ToString());
			}
		}
	}
}