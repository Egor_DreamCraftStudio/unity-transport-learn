using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Networking.Transport;
using UnityEngine;

namespace Multiplayer
{
	public class Server : MonoBehaviour
	{
		const int c_initConnectionsCapacity = 16;

		[SerializeField] bool _useLocalHost = true;
		[SerializeField] string _ip;

		MultiNetworkDriver _driver;
		NativeList<NetworkConnection> _connections;

		List<ICommandReaction> _commandReactions;

		void Update()
		{
			_driver.ScheduleUpdate().Complete();
			CleanUpConnections();
			AcceptNewConnections();
			ProcessConnections();
		}

		void OnDestroy()
		{
			if (_driver.IsCreated)
			{
				_driver.Dispose();
				_connections.Dispose();
			}
		}

		public void Init()
		{
			if (TryCreateEndPoint(out var endpoint) == false)
				return;

			if (TryCreateMultiNetworkDriver(ref endpoint) == false)
				return;

			_connections = new NativeList<NetworkConnection>(
				c_initConnectionsCapacity, Allocator.Persistent);

			_commandReactions = new List<ICommandReaction>()
				{ new LogCommand(), new MessageReceiver() };
		}

		public void SendSomeCommand()
		{
			// _driver.BeginSend(NetworkPipeline.Null, _connections[i],
			// 	out var writer);
			// writer.WriteUInt(number);
			// _driver.EndSend(writer);
		}

		bool TryCreateEndPoint(out NetworkEndpoint endpoint)
		{
			if (_useLocalHost)
			{
				endpoint = NetworkEndpoint.AnyIpv4.WithPort(Const.c_Port);
				return true;
			}

			if (NetworkEndpoint.TryParse(_ip, Const.c_Port, out endpoint))
				return true;

			Debug.LogError($"Incorrect ip: \"{_ip}\".");
			return false;
		}

		void ProcessConnections()
		{
			for (var i = 0; i < _connections.Length; i++)
				ProcessConnection(i);
		}

		void ProcessConnection(int i)
		{
			NetworkEvent.Type cmd;
			while ((cmd = _driver.PopEventForConnection(_connections[i],
				       out DataStreamReader stream)) != NetworkEvent.Type.Empty)
			{
				if (ProcessDisconnect(i, cmd))
					break;

				ProcessNetworkEvents(i, cmd, ref stream);
			}
		}

		bool ProcessDisconnect(int i, NetworkEvent.Type cmd)
		{
			if (cmd == NetworkEvent.Type.Disconnect)
			{
				Debug.Log("Client disconnected from the server.");
				_connections[i] = default;
				return true;
			}

			return false;
		}

		void ProcessNetworkEvents(int i, NetworkEvent.Type cmd,
			ref DataStreamReader stream)
		{
			switch (cmd)
			{
				case NetworkEvent.Type.Connect:
					ProcessConnect();
					break;
				case NetworkEvent.Type.Data:
					ProcessData(i, ref stream);
					break;
				case NetworkEvent.Type.Empty:
					Debug.LogWarning($"Has not handler for \"{cmd}\".");
					break;
			}
		}

		void ProcessData(int i, ref DataStreamReader stream)
		{
			var command = Util.GetCommand(ref stream);
			foreach (var reaction in _commandReactions)
				reaction.TryReact(command, ref stream);
		}

		void ProcessConnect()
		{
			Debug.Log("We are now connected to the client.");
		}

		void CleanUpConnections()
		{
			for (int i = 0; i < _connections.Length; i++)
				if (_connections[i].IsCreated == false)
					_connections.RemoveAtSwapBack(i--);
		}

		void AcceptNewConnections()
		{
			NetworkConnection c;
			while ((c = _driver.Accept()) != default)
			{
				_connections.Add(c);
				Debug.Log("Accepted a connection.");
			}
		}

		bool TryCreateMultiNetworkDriver(ref NetworkEndpoint endpoint)
		{
			_driver = MultiNetworkDriver.Create();

			if (TryCreateDriver(new UDPNetworkInterface(), ref endpoint) == false)
			{
				_driver.Dispose();
				return false;
			}

			if (TryCreateDriver(new WebSocketNetworkInterface(), ref endpoint) ==
			    false)
			{
				_driver.Dispose();
				return false;
			}

			return true;
		}

		bool TryCreateDriver<TNetworkInterface>(TNetworkInterface networkInterface,
			ref NetworkEndpoint endpoint)
			where TNetworkInterface : unmanaged, INetworkInterface
		{
			var networkDriver = NetworkDriver.Create(networkInterface);
			if (networkDriver.Bind(endpoint) != 0 || networkDriver.Listen() != 0)
			{
				Debug.LogError(
					$"Failed to bind or listen to {networkDriver} port {Const.c_Port}.");
				networkDriver.Dispose();
				return false;
			}

			_driver.AddDriver(networkDriver);
			return true;
		}
	}
}