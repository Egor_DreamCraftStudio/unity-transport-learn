﻿using System;
using Unity.Collections;
using UnityEngine;

namespace Multiplayer
{
	public sealed class LogCommand : ICommandReaction
	{
		public void TryReact(Command command, ref DataStreamReader stream)
		{
			var commandName = Enum.GetName(typeof(Command), command);
			Debug.Log(commandName);
		}
	}
}