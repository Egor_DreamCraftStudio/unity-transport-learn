﻿using System;
using Unity.Collections;
using UnityEngine;

namespace Multiplayer
{
	public static class Util
	{
		public static Command GetCommand(ref DataStreamReader stream)
		{
			var command = (Command)stream.ReadUShort();
			var isDefinedCommand = Enum.IsDefined(typeof(Command), command);
			if (isDefinedCommand)
				return command;

			Debug.LogError($"Unknown command: \"{command}\"");
			return Command.None;
		}
	}
}